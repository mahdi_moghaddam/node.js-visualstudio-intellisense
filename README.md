Node.js VisualStudio IntelliSense
===

Node.js VisualStudio IntelliSense

- Require VisualStudio 2012b
- This library adds the definition of Node.js to VisualStudio. 

##Installation

	The nodelib folder import to your project

##Usage

your javascript file

	/// <reference path="./nodelib/node.js"/>

	--your codes

##History

###v0.0.2: 2012/06/15
- The addition of function argument type.

###v0.0.1: 2012/06/07
- Release
- Target Node.js viersion 0.7.9

